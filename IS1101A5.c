#include<stdio.h>

#define CostOfPerformance 500
#define PerAttendee 3

int NumOfAttendees(int price);
int Revenue(int price);
int Cost(int price);
int Profit(int price);

int NumOfAttendees(int price){
return 120-(price-15)/5*20;
}

int Revenue(int price){
return NumOfAttendees(price)*price;
}

int Cost(int price){
return NumOfAttendees(price)*PerAttendee+CostOfPerformance;
}

int Profit(int price){
return Revenue(price)-Cost(price);
}

int main() {
int price;
printf("\nProfit for Ticket Prices: \n");

for(price=5;price<=45;price+=5){
    printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,Profit(price));
} return 0;

}
